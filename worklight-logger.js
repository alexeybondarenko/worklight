'use strict';

angular.module("worklight.logger", [])
    .factory("WorklightLogger", WorklightLogger);

WorklightLogger.$inject = [];
function WorklightLogger () {
    return function (name) {
        return typeof WL != "undefined" ? WL.Logger.create({pkg:name}) : {
            info : function(msg) {console.info(name + ": " +msg);},
            log : function(msg) {console.log(name + ": " +msg);},
            debug : function(msg) {console.debug(name + ": " +msg);},
            trace : function(msg) {console.trace(name + ": " +msg);}
        };
    };
}