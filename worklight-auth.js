(function(angular) {
    'use strict';

    var app = angular.module("worklight.auth", ['worklight.logger']);

    app.factory("WorklightAdapterAuthService", WorklightAdapterAuthService);
    WorklightAdapterAuthService.$inject = ['$rootScope', '$q', 'WorklightLogger'];

    function WorklightAdapterAuthService ($rootScope, $q, WorklightLogger) {

        var l = new WorklightLogger("WorklightAdapterAuthService");

        /**
         * Service of the authentication for the Worklight Server authentication features.
         * @param config
         */
        var authService = function(config) {
            config = config || {};
            l.info('constructor: init');
            if (typeof config.realmName == "undefined" || !config.realmName) l.debug("Creation AuthService instance with undefined realm name");
            this.realmName = config.realmName || 'AdapterAuthRealm';
            this.adapterName = 'AuthAdapter';
            this.submitAuthProcedureName = 'submitAuth';
        };
        /**
         * Name of the realm
         * @type {null}
         */
        authService.prototype.realmName = null;
        /**
         * Name of the adapter for the adapter based authentication
         * @type {null}
         */
        authService.prototype.adapterName = null;
        /**
         * Name of the submit auth procedure
         * @type {null}
         */
        authService.prototype.submitAuthProcedureName = null;

        var challengeHandler = (typeof WL != "undefined") ? new WL.Client.createChallengeHandler() : {};
        challengeHandler.isCustomResponse = function(resp){
            var res = resp && resp.responseJSON && typeof (resp.responseJSON.authStatus) === "string";
            l.info('isCustomResponse: '+res);
            return res;
        };
        /**
         * Handling the response from the server
         * @param resp - response from the Worklight Server
         */
        challengeHandler.handleChallenge = function(resp){
            l.info('challengeHandler.handleChallenge: init');
            var authStatus = resp.responseJSON.authStatus;
            if (authStatus === "required"){
                l.debug('challengeHandler.handleChallenge: loginRequiredEvent');
                $rootScope.$broadcast("loginRequiredEvent", resp);
            } else if (authStatus === "complete"){
                l.debug("challengeHandler.handleChallenge:: loginCompleteEvent");
                $rootScope.$broadcast("loginCompleteEvent", resp);
                challengeHandler.submitSuccess();
            }
        };
        /**
         * Test function for requiring authentication and init j_security check
         */
        authService.prototype.isAuth = function() {
            l.info("AuthService::isAuth init");

            var defer = $q.defer(),
                invocationOptions = {
                    adapter: "AuthAdapter",
                    procedure: "isAuth",
                    parameters: []
                };

            if (typeof WL!= "undefined") {
                // Hot fix for auth requests bug
                WL.Client.invokeProcedure(invocationOptions,{
                    onSuccess: function(resp) {
                        l.debug("isAuth:: success callback");
                        defer.resolve(resp);
                    },
                    onFailure: function(err) {
                        l.debug("isAuth:: failure callback");
                        defer.reject(err);
                    }
                });
            }
            else defer.reject();

            return defer.promise;
        };
        /**
         * Logout method
         * @returns {*}
         */
        authService.prototype.logout = function() {
            var self = this,
                defer = $q.defer();
            l.info("logout: init");
            if (typeof WL!= "undefined")
                WL.Client.logout(this.realmName, {
                    onSuccess: function(resp) {
                        l.debug("AuthService::logout success");
                        defer.resolve(resp);
                        self.isAuth();
                    },
                    onFailure: function(err) {
                        l.debug("AuthService::logout failure");
                        defer.reject(err);
                    }
                });
            else {
                l.debug("logout: WL is undefined");
                defer.reject();
            }

            return defer.promise;
        };
        /**
         * Login method
         * @param username
         * @param password
         */
        authService.prototype.login = function(username, password){

            var defer = $q.defer(),
                invocationData = {
                    adapter: this.adapterName,
                    procedure: 'submitAuth',
                    parameters : [username, password]
                };
            l.info("login: init");
            if (typeof WL != "undefined")
                challengeHandler.submitAdapterAuthentication(invocationData, {
                    onSuccess: function(resp) {
                        l.debug("login success callback");
                        defer.resolve(resp);
                    },
                    onFailure: function(err) {
                        l.debug("login failure callback");
                        defer.reject(err);
                    }
                });
            else {
                l.debug("logout: WL is undefined");
                defer.reject();
            }

            return defer.promise;
        };
        /**
         * Get the user name
         * @returns {*}
         */
        authService.prototype.getUserName = function() {
            l.info("getUserName: init");
            if (typeof WL == "undefined" || !WL.Client) return null;
            return WL.Client.getUserName(this.realmName);
        };

        return authService;
    }

    app.factory("WorklightAuthLDAPService",WorklightAuthLDAPService);
    WorklightAuthLDAPService.$inject = ['$rootScope', '$q', 'WorklightLogger'];
    function WorklightAuthLDAPService ($rootScope, $q, WorklightLogger) {

        var l = new WorklightLogger("WorklightAuthLDAPService");

        var _isAuth = false;
        /**
         * Service of the authentication for the Worklight Server authentication features.
         * @param config
         */
        var authService = function(config) {
            config = config || {};
            l.info('constructor: init');
            if (typeof config.realmName == "undefined" || !config.realmName) l.debug("Creation AuthService instance with undefined realm name");
            this.realmName = config.realmName || 'LDAPRealm';

            var challengeHandler = {};
            try {
                challengeHandler = new WL.Client.createChallengeHandler(this.realmName);
            } catch (e) {}

            challengeHandler.handleChallenge = handleChallenge;
            challengeHandler.isCustomResponse = isCustomResponse;

            this.challengeHandler = challengeHandler;

        };
        /**
         * Name of the realm
         * @type {null}
         */
        authService.prototype.realmName = null;

        authService.prototype.challengeHandler = null;

        function isCustomResponse (resp) {
            if (!resp || !resp.responseText) return false;
            if (resp.status !== 200 && resp.status !== 0) return true;

            var idx = resp.responseText.indexOf('j_security_check');
            return idx >= 0;
        }
        /**
         * Handling the response from the server
         * @param resp - response from the Worklight Server
         */
        function handleChallenge (resp) {
            l.info('challengeHandler.handleChallenge: init');
            l.debug('challengeHandler.handleChallenge: loginRequiredEvent');
            $rootScope.$broadcast("loginRequiredEvent", resp);
            _isAuth = false;
        }
        /**
         * Test function for requiring authentication and init j_security check
         */
        authService.prototype.isAuth = function() {
            l.info("AuthService::isAuth init");
            var defer = $q.defer();

            try {
                if (typeof WL === 'undefined') throw "Undefined WL";
                if (typeof WL.Client === 'undefined') throw "Undefined WL.Client";
                if (typeof this.realmName === 'undefined') throw "Undefined realm name";

                _isAuth = WL.Client.isUserAuthenticated(this.realmName);
            } catch (e) {
                l.debug("isAuth error: "+e);
            }

            if (_isAuth) $rootScope.$broadcast("loginCompleteEvent", _isAuth);
            else $rootScope.$broadcast("loginRequiredEvent", _isAuth);

            defer.resolve(_isAuth);

            return defer.promise;
        };
        /**
         * Logout method
         * @returns {*}
         */
        authService.prototype.logout = function() {
            var self = this,
                defer = $q.defer();
            l.info("logout: init");
            try {
                if (typeof WL === "undefined") throw "Undefined WL";
                if (typeof WL.Client === "undefined") throw "Undefined WL.Client";
                if (typeof this.realmName === "undefined") throw "Undefined realmName";

                WL.Client.logout(this.realmName, {
                    onSuccess: function(resp) {
                        l.debug("AuthService::logout success");
                        defer.resolve(resp);
                        self.isAuth();
                    },
                    onFailure: function(err) {
                        l.debug("AuthService::logout failure");
                        defer.reject(err);
                    }
                });

            } catch (e) {
                l.debug("logout:: "+ e);
                defer.reject();
            }

            return defer.promise;
        };
        /**
         * Login method
         * @param username
         * @param password
         */
        authService.prototype.login = function(username, password){

            l.info("login: init");
            console.log(username, password);
            var defer = $q.defer(),
                reqURL = '/j_security_check',
                options = {};

            options.parameters = {
                j_username: username,
                j_password: password
            };
            options.header = {};

            var challengeHandler = this.challengeHandler;
            try {
                if (typeof WL === 'undefined') throw "Undefined WL";
                WL.Client.login(this.realmName, {
                    onSuccess: function(resp) {
                        $rootScope.$broadcast("loginCompleteEvent", resp);
                        console.log("WL.Client.login success", arguments);
                    },
                    onFailure: function() {
                        console.log("WL.Client.login failure", arguments);
                    }
                });
                challengeHandler.submitLoginForm(reqURL, options, function (resp) {
                    l.info("submitLoginFormCallback");
                    var isCustomResp = challengeHandler.isCustomResponse(resp);
                    if (isCustomResp) {
                        challengeHandler.handleChallenge(resp);
                        defer.reject(resp);
                    } else {
                        l.debug("challengeHandler.submitLoginFormCallback:: loginCompleteEvent");
                        defer.resolve(resp);
                        challengeHandler.submitSuccess();
                        _isAuth = true;
                    }
                });
            } catch (e) {
                l.debug("logout: "+e);
                defer.reject();
            }

            return defer.promise;
        };
        /**
         * Get the user name
         * @returns {*}
         */
        authService.prototype.getUserName = function() {
            l.info("getUserName: init");
            var username = null;

            try {
                if (typeof WL === 'undefined') throw "Undefined WL";
                if (typeof WL.Client === 'undefined') throw "Undefined WL.Client";
                username = WL.Client.getUserName(this.realmName);
            } catch (e) {
                l.debug("GetUserName error: "+ e);
            }

            return username;
        };

        return authService;
    }
}(angular));
