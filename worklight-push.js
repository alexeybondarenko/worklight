(function() {
    'use strict';

    var app = angular.module("worklight.push", []);

//    app.run(function($rootScope) { });

    app.factory('PushService', ['$q','$rootScope', function ($q, $rootScope) {
        /**
         * Push object for Worklight Project
         * @constructor
         */
        var Push = function (name, callback, config) {
            this.name = name || 'myPush';
            config = config || {};

            this.pushAdapterName = config.pushAdapterName || "PushAdapter";
            this.pushEventSourceName = config.pushEventSourceName || "PushEventSource";
            this.pushSubmitProcedure = config.pushSubmitProcedure || "submitNotification";

            this.init(name, this.pushAdapterName, this.pushEventSourceName, callback);
        };
        /**
         * Name of the Push subscription on Worklight Server
         * @property name
         * @type {string}
         */
        Push.prototype.name = null;
        /**
         * Name if the push Adapter
         * @type {string}
         */
        Push.prototype.pushAdapterName = null;
        /**
         * Name of the push event source name
         * @type {string}
         */
        Push.prototype.pushEventSourceName = null;
        /**
         * Name pf the push submit procedure
         * @type {string}
         */
        Push.prototype.pushSubmitProcedure = null;
        /**
         * Subscribing method
         * @method Push.subscribe
         * @returns {promise}
         */
        Push.prototype.subscribe = function () {
            var def = $q.defer();
            console.log("PushService:: subscribe");
            if (!this.isSupported()) {
                (function() {
                    def.reject();
                }());
                return def.promise;
            }
            WL.Client.Push.subscribe(this.name, {
                onSuccess: function (response) {
                    console.log("Success: ", response);
                    def.resolve(response);
                },
                onFailure: function (error) {
                    console.warn("Error: ", error);
                    def.reject(error);
                }
            });
            return def.promise;
        };
        /**
         * Unsibscribing push method
         * @method Push.unsubscribe
         * @returns {promise}
         */
        Push.prototype.unsubscribe = function () {
            var defer = $q.defer();
            console.log("PushService:: unsubscribe");
            if (!this.isSupported()) {
                (function() {
                    defer.reject();
                }());
                return defer.promise;
            }
            WL.Client.Push.unsubscribe(this.name, {
                onSuccess: function (response) {
                    console.log("Success: ", response);
                    defer.resolve(response);
                },
                onFailure: function (error) {
                    console.warn("Error: ", error);
                    defer.reject(error);
                }
            });
            return defer.promise;
        };
        /**
         * Test for supporting push notification by device
         * @returns {boolean}
         */
        Push.prototype.isSupported = function () {
            var isSupported = false;
            if (typeof WL != 'undefined' && WL.Client.Push){
                isSupported = WL.Client.Push.isPushSupported();
            }
            return isSupported;
        };
        /**
         * Test for subscribed or not subscribed device for push notifications
         * @returns {boolean}
         */
        Push.prototype.isSubscribed = function () {
            var isSubscribed = false;
            if (typeof WL != 'undefined' && WL.Client.Push){
                isSubscribed = WL.Client.Push.isSubscribed(this.name);
            }
            return isSubscribed;
        };
        Push.prototype.submit = function(userID, message) {
            var defer = $q.defer();

            if (!this.isSupported() || typeof userID == "undefined") {
                (function() {
                    defer.reject();
                }());
                return defer.promise;
            }

            var invocationOptions = {
                adapter: this.pushAdapterName,
                procedure: this.pushSubmitProcedure,
                parameters: [userID, message]
            };

            WL.Client.invokeProcedure(invocationOptions,{
                onSuccess: function(resp) {
                    defer.resolve(resp);
                },
                onFailure: function(err) {
                    console.log(err);
                    defer.reject(err);
                }
            });
            return defer.promise;
        };

        /**
         * Callback function for received notifications
         * @param props
         * @param payload
         */
        function callbackFunction (props, payload) {
            console.log("Получено Push уведомление. Подробности в логах.", props, payload);
            $rootScope.$broadcast('pushReceived', {
                props: props,
                payload: payload
            });
        }
        Push.prototype.init = function (pushName, pushAdapterName, pushEventSourceName, pushEventCallbackFunc) {
            pushAdapterName = pushAdapterName || "PushAdapter";
            pushEventSourceName = pushEventSourceName || "PushEventSource";
            pushEventCallbackFunc = pushEventCallbackFunc || function(){};

            if (typeof WL != "undefined" && WL.Client.Push) {
                console.log("WL.Client.Push is ready");
                // Subscribe on Push then device is ready
                WL.Client.Push.onReadyToSubscribe = function () {
                    console.log("App is ready to subscribe on Push");

                    // registering event source callback functions for push notifications
                    WL.Client.Push.registerEventSourceCallback(
                        pushName, // Push subscribtion name
                        pushAdapterName, // Push notification adapter name
                        pushEventSourceName, // Name of the push event source
                        function(props, payload) { // Callback function for receiving notifications
                            callbackFunction(props, payload);
                            pushEventCallbackFunc(props,payload);
                        });
                };
            }
        };


        return Push;
    }]);
}());
